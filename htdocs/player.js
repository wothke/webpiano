/**
* Facade used to wrap the ScriptNodePlayer emulator plugin.
*
* <p>This is not meant to be reusable but it might give an idea how the ScriptNodePlayer is used.
*/

class Player {
	constructor(redrawGraphCallback, nextFrameCallback, tracer)
	{
		this.redrawGraphCallback = redrawGraphCallback;

		this.backend = new PianoBackendAdapter(nextFrameCallback);
		this.backend.setProcessorBufSize(4096);

		ScriptNodePlayer.createInstance(this.backend, "", [], false, this.doOnPlayerReady.bind(this),
										this.doOnTrackReadyToPlay.bind(this), this.doOnTrackEnd.bind(this), undefined, tracer, undefined);
	}

	doOnTrackReadyToPlay()
	{
	//	ScriptNodePlayer.getInstance().setSilenceTimeout(0);
		this.redrawGraphCallback();
	}

	getBackend()
	{
		return this.backend;
	}

	doOnTrackEnd()
	{
		this.playSong();
	}

	doOnPlayerReady()
	{
		this.doOnTrackEnd(); // player are used to check for init()
	}

	pause() 			{ ScriptNodePlayer.getInstance().pause(); }
	resume() 			{ ScriptNodePlayer.getInstance().resume(); }
	setVolume(value) 	{ ScriptNodePlayer.getInstance().setVolume(value); }
	getSongInfo ()		{ return ScriptNodePlayer.getInstance().getSongInfo(); }

	playSong()
	{
		let audioCtx = ScriptNodePlayer.getWebAudioContext();	// handle Google's bullshit "autoplay policy"
		if (audioCtx.state == "suspended")
		{
			let modal = document.getElementById('autoplayConfirm');
			modal.style.display = "block";		// force user to click

			window.globalDeferredPlay = function() {	// setup function to be used "onClick"
				audioCtx.resume();
				this._playSong();
			}.bind(this);

		}
		else
		{
			this._playSong();
		}
	}

	_playSong()
	{
		let options= {};
		ScriptNodePlayer.getInstance().loadMusicFromURL(null, options);	// player autoplays after load (use any dummy filename except "")
	}

};
