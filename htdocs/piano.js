/*
* piano-widget used for WebSID Studio screen
*
* 	Copyright (C) 2020 Juergen Wothke
*/
class Piano {
	constructor(callback) {		
		this.keyColors = 		["#fff", "#000", "#fff", "#000", "#fff", "#fff", "#000", "#fff", "#000", "#fff", "#000", "#fff"];
		this.keyColorsPressed = ["#ddd", "#666", "#ddd", "#666", "#ddd", "#ddd", "#666", "#ddd", "#666", "#ddd", "#666", "#ddd"];
		this.notes2Idx = 		[0,2,4,5,7,9,11];

		this.prevKey= null;
		this.prevOctave= null;
		this.callback= callback;
	}
	inject(divSelect) {
		$(divSelect).html(
		'<svg id="o1" width="256" height="256" viewbox="0 0 77.5 76.9" style="position:absolute;left:0px;" xmlns="http://www.w3.org/2000/svg" xmlns:svg="http://www.w3.org/2000/svg">'+
			'<path class="white" id="o0k11" d="m55.75595,49.71429c0,1.104 0.896,2 2,2l5,0c1.104,0 2,-0.896 2,-2l0,-46c0,-1.104 -0.896,-2 -2,-2l-5,0c-1.104,0 -2,0.896 -2,2l0,46z" fill="#fff"/>'+
			'<path d="m55.75595,49.71429c0,1.104 0.896,2 2,2l5,0c1.104,0 2,-0.896 2,-2l0,-46c0,-1.104 -0.896,-2 -2,-2l-5,0c-1.104,0 -2,0.896 -2,2l0,46z" fill="none" stroke="#000"/>'+
			'<path class="white" id="o0k9" d="m46.75595,49.71429c0,1.104 0.896,2 2,2l5,0c1.104,0 2,-0.896 2,-2l0,-46c0,-1.104 -0.896,-2 -2,-2l-5,0c-1.104,0 -2,0.896 -2,2l0,46z" fill="#fff"/>'+
			'<path d="m46.75595,49.71429c0,1.104 0.896,2 2,2l5,0c1.104,0 2,-0.896 2,-2l0,-46c0,-1.104 -0.896,-2 -2,-2l-5,0c-1.104,0 -2,0.896 -2,2l0,46z" fill="none" stroke="#000"/>'+
			'<path class="white" id="o0k7" d="m37.75595,49.71429c0,1.104 0.896,2 2,2l5,0c1.104,0 2,-0.896 2,-2l0,-46c0,-1.104 -0.896,-2 -2,-2l-5,0c-1.104,0 -2,0.896 -2,2l0,46z" fill="#fff"/>'+
			'<path d="m37.75595,49.71429c0,1.104 0.896,2 2,2l5,0c1.104,0 2,-0.896 2,-2l0,-46c0,-1.104 -0.896,-2 -2,-2l-5,0c-1.104,0 -2,0.896 -2,2l0,46z" fill="none" stroke="#000"/>'+
			'<path class="white" id="o0k5" d="m28.75595,49.71429c0,1.104 0.896,2 2,2l5,0c1.104,0 2,-0.896 2,-2l0,-46c0,-1.104 -0.896,-2 -2,-2l-5,0c-1.104,0 -2,0.896 -2,2l0,46z" fill="#fff"/>'+
			'<path d="m28.75595,49.71429c0,1.104 0.896,2 2,2l5,0c1.104,0 2,-0.896 2,-2l0,-46c0,-1.104 -0.896,-2 -2,-2l-5,0c-1.104,0 -2,0.896 -2,2l0,46z" fill="none" stroke="#000"/>'+
			'<path class="white" id="o0k4" d="m19.75595,49.71429c0,1.104 0.896,2 2,2l5,0c1.104,0 2,-0.896 2,-2l0,-46c0,-1.104 -0.896,-2 -2,-2l-5,0c-1.104,0 -2,0.896 -2,2l0,46z" fill="#fff"/>'+
			'<path d="m19.75595,49.71429c0,1.104 0.896,2 2,2l5,0c1.104,0 2,-0.896 2,-2l0,-46c0,-1.104 -0.896,-2 -2,-2l-5,0c-1.104,0 -2,0.896 -2,2l0,46z" fill="none" stroke="#000"/>'+
			'<path class="white" id="o0k2" d="m10.75595,49.71429c0,1.104 0.896,2 2,2l5,0c1.104,0 2,-0.896 2,-2l0,-46c0,-1.104 -0.896,-2 -2,-2l-5,0c-1.104,0 -2,0.896 -2,2l0,46z" fill="#fff"/>'+
			'<path d="m10.75595,49.71429c0,1.104 0.896,2 2,2l5,0c1.104,0 2,-0.896 2,-2l0,-46c0,-1.104 -0.896,-2 -2,-2l-5,0c-1.104,0 -2,0.896 -2,2l0,46z" fill="none" stroke="#000"/>'+
			'<path class="white" id="o0k0" d="m1.75595,49.71429c0,1.104 0.896,2 2,2l5,0c1.104,0 2,-0.896 2,-2l0,-46c0,-1.104 -0.896,-2 -2,-2l-5,0c-1.104,0 -2,0.896 -2,2l0,46z" fill="#fff"/>'+
			'<path d="m1.75595,49.71429c0,1.104 0.896,2 2,2l5,0c1.104,0 2,-0.896 2,-2l0,-46c0,-1.104 -0.896,-2 -2,-2l-5,0c-1.104,0 -2,0.896 -2,2l0,46z" fill="none" stroke="#000"/>'+
			'<rect class="black" id="o0k1" height="33" width="4.625" y="1.71429" x="7.71429" stroke-linecap="null" stroke-linejoin="null" stroke-dasharray="null" stroke-width="null" stroke="#000" fill="#000"/>'+
			'<rect class="black" id="o0k3" height="33" width="4.625" y="1.75595" x="18.06845" stroke-linecap="null" stroke-linejoin="null" stroke-dasharray="null" stroke-width="null" stroke="#000" fill="#000"/>'+
			'<rect class="black" id="o0k6" height="33" width="4.5" y="1.75595" x="34.81845" stroke-linecap="null" stroke-linejoin="null" stroke-dasharray="null" stroke-width="null" stroke="#000" fill="#000"/>'+
			'<rect class="black" id="o0k8" height="33" width="4.375" y="1.75595" x="44.56845" stroke-linecap="null" stroke-linejoin="null" stroke-dasharray="null" stroke-width="null" stroke="#000" fill="#000"/>'+
			'<rect class="black" id="o0k10" height="33" width="4.5" y="1.75595" x="54.19345" stroke-linecap="null" stroke-linejoin="null" stroke-dasharray="null" stroke-width="null" stroke="#000" fill="#000"/>'+
			'<rect class="filet ff0" fill="#000" stroke="#000" stroke-width="null" stroke-dasharray="null" stroke-linejoin="null" stroke-linecap="null" x="1.71429" y="1.71429" width="63.0625" height="1.625"/>'+
		'</svg>'+
		'<svg id="o2" width="256" height="256" viewbox="0 0 77.5 76.9" style="position:absolute;left:208px;" xmlns="http://www.w3.org/2000/svg" xmlns:svg="http://www.w3.org/2000/svg">'+
			'<path class="white" id="o1k11" d="m55.75595,49.71429c0,1.104 0.896,2 2,2l5,0c1.104,0 2,-0.896 2,-2l0,-46c0,-1.104 -0.896,-2 -2,-2l-5,0c-1.104,0 -2,0.896 -2,2l0,46z" fill="#fff"/>'+
			'<path d="m55.75595,49.71429c0,1.104 0.896,2 2,2l5,0c1.104,0 2,-0.896 2,-2l0,-46c0,-1.104 -0.896,-2 -2,-2l-5,0c-1.104,0 -2,0.896 -2,2l0,46z" fill="none" stroke="#000"/>'+
			'<path class="white" id="o1k9" d="m46.75595,49.71429c0,1.104 0.896,2 2,2l5,0c1.104,0 2,-0.896 2,-2l0,-46c0,-1.104 -0.896,-2 -2,-2l-5,0c-1.104,0 -2,0.896 -2,2l0,46z" fill="#fff"/>'+
			'<path d="m46.75595,49.71429c0,1.104 0.896,2 2,2l5,0c1.104,0 2,-0.896 2,-2l0,-46c0,-1.104 -0.896,-2 -2,-2l-5,0c-1.104,0 -2,0.896 -2,2l0,46z" fill="none" stroke="#000"/>'+
			'<path class="white" id="o1k7" d="m37.75595,49.71429c0,1.104 0.896,2 2,2l5,0c1.104,0 2,-0.896 2,-2l0,-46c0,-1.104 -0.896,-2 -2,-2l-5,0c-1.104,0 -2,0.896 -2,2l0,46z" fill="#fff"/>'+
			'<path d="m37.75595,49.71429c0,1.104 0.896,2 2,2l5,0c1.104,0 2,-0.896 2,-2l0,-46c0,-1.104 -0.896,-2 -2,-2l-5,0c-1.104,0 -2,0.896 -2,2l0,46z" fill="none" stroke="#000"/>'+
			'<path class="white" id="o1k5" d="m28.75595,49.71429c0,1.104 0.896,2 2,2l5,0c1.104,0 2,-0.896 2,-2l0,-46c0,-1.104 -0.896,-2 -2,-2l-5,0c-1.104,0 -2,0.896 -2,2l0,46z" fill="#fff"/>'+
			'<path d="m28.75595,49.71429c0,1.104 0.896,2 2,2l5,0c1.104,0 2,-0.896 2,-2l0,-46c0,-1.104 -0.896,-2 -2,-2l-5,0c-1.104,0 -2,0.896 -2,2l0,46z" fill="none" stroke="#000"/>'+
			'<path class="white" id="o1k4" d="m19.75595,49.71429c0,1.104 0.896,2 2,2l5,0c1.104,0 2,-0.896 2,-2l0,-46c0,-1.104 -0.896,-2 -2,-2l-5,0c-1.104,0 -2,0.896 -2,2l0,46z" fill="#fff"/>'+
			'<path d="m19.75595,49.71429c0,1.104 0.896,2 2,2l5,0c1.104,0 2,-0.896 2,-2l0,-46c0,-1.104 -0.896,-2 -2,-2l-5,0c-1.104,0 -2,0.896 -2,2l0,46z" fill="none" stroke="#000"/>'+
			'<path class="white" id="o1k2" d="m10.75595,49.71429c0,1.104 0.896,2 2,2l5,0c1.104,0 2,-0.896 2,-2l0,-46c0,-1.104 -0.896,-2 -2,-2l-5,0c-1.104,0 -2,0.896 -2,2l0,46z" fill="#fff"/>'+
			'<path d="m10.75595,49.71429c0,1.104 0.896,2 2,2l5,0c1.104,0 2,-0.896 2,-2l0,-46c0,-1.104 -0.896,-2 -2,-2l-5,0c-1.104,0 -2,0.896 -2,2l0,46z" fill="none" stroke="#000"/>'+
			'<path class="white" id="o1k0" d="m1.75595,49.71429c0,1.104 0.896,2 2,2l5,0c1.104,0 2,-0.896 2,-2l0,-46c0,-1.104 -0.896,-2 -2,-2l-5,0c-1.104,0 -2,0.896 -2,2l0,46z" fill="#fff"/>'+
			'<path d="m1.75595,49.71429c0,1.104 0.896,2 2,2l5,0c1.104,0 2,-0.896 2,-2l0,-46c0,-1.104 -0.896,-2 -2,-2l-5,0c-1.104,0 -2,0.896 -2,2l0,46z" fill="none" stroke="#000"/>'+
			'<rect class="black" id="o1k1" height="33" width="4.625" y="1.71429" x="7.71429" stroke-linecap="null" stroke-linejoin="null" stroke-dasharray="null" stroke-width="null" stroke="#000" fill="#000"/>'+
			'<rect class="black" id="o1k3" height="33" width="4.625" y="1.75595" x="18.06845" stroke-linecap="null" stroke-linejoin="null" stroke-dasharray="null" stroke-width="null" stroke="#000" fill="#000"/>'+
			'<rect class="black" id="o1k6" height="33" width="4.5" y="1.75595" x="34.81845" stroke-linecap="null" stroke-linejoin="null" stroke-dasharray="null" stroke-width="null" stroke="#000" fill="#000"/>'+
			'<rect class="black" id="o1k8" height="33" width="4.375" y="1.75595" x="44.56845" stroke-linecap="null" stroke-linejoin="null" stroke-dasharray="null" stroke-width="null" stroke="#000" fill="#000"/>'+
			'<rect class="black" id="o1k10" height="33" width="4.5" y="1.75595" x="54.19345" stroke-linecap="null" stroke-linejoin="null" stroke-dasharray="null" stroke-width="null" stroke="#000" fill="#000"/>'+
			'<rect class="filet ff0" fill="#000" stroke="#000" stroke-width="null" stroke-dasharray="null" stroke-linejoin="null" stroke-linecap="null" x="1.71429" y="1.71429" width="63.0625" height="1.625"/>'+
		'</svg>'		
		);
		
		$('#o1').mousedown(this.onClickKey.bind(this));
		$('#o2').mousedown(this.onClickKey.bind(this));
		$('#o1').mouseup(this.onReleaseKey.bind(this));
		$('#o2').mouseup(this.onReleaseKey.bind(this));

		$(document).keyup(this.onReleaseKey.bind(this));
		$(document).keypress(this.onKeyboardPress.bind(this));	
		
	}
	pressKey(octave, keyIdIdx) {
		if (this.prevKey == keyIdIdx) return;

		// only allow one key at a time.. so it can be easily mapped to 1 or 2 SID voices
		if (keyIdIdx != null)
			$("#o"+octave+"k"+keyIdIdx).css("transition", "fill .05s linear").attr("fill", this.keyColorsPressed[keyIdIdx]);
//		else
//			release= 1;
			
		// clear previous key
		if (this.prevKey != null) $("#o"+this.prevOctave+"k"+this.prevKey).css("transition", "none").attr("fill", this.keyColors[this.prevKey]);
		
		this.prevKey = keyIdIdx;
		this.prevOctave = octave;

		var voice= 0;	// only use 1 
		this.callback(voice, 4+octave, keyIdIdx, 2.5);
	}
	onKeyboardPress(event) {
		switch (event.charCode) {
			// octave 0
			case 97:	//a
				this.pressKey(0, 0); break;
			case 119:	//w
				this.pressKey(0, 1); break;
			case 115:	//s
				this.pressKey(0, 2); break;
			case 101:	//e
				this.pressKey(0, 3); break;
			case 100:	//d		
				this.pressKey(0, 4); break;
			case 102:	//f
				this.pressKey(0, 5); break;
			case 116:	//t
				this.pressKey(0, 6); break;
			case 103:	//g
				this.pressKey(0, 7); break;
			case 122:	//z		QWERZ
			case 121:	//y		QWERY
				this.pressKey(0, 8); break;
			case 104:	//h
				this.pressKey(0, 9); break;
			case 117:	//u
				this.pressKey(0, 10); break;
			case 106:	//j
				this.pressKey(0, 11); break;
			// octave 1
			case 97-32:		//A
				this.pressKey(1, 0); break;
			case 119-32:	//W
				this.pressKey(1, 1); break;
			case 115-32:	//S
				this.pressKey(1, 2); break;
			case 101-32:	//E
				this.pressKey(1, 3); break;
			case 100-32:	//D		
				this.pressKey(1, 4); break;
			case 102-32:	//F
				this.pressKey(1, 5); break;
			case 116-32:	//T
				this.pressKey(1, 6); break;
			case 103-32:	//G
				this.pressKey(1, 7); break;
			case 122-32:	//Z		QWERZ
			case 121-32:	//Y		QWERY
				this.pressKey(1, 8); break;
			case 104-32:	//H
				this.pressKey(1, 9); break;
			case 117-32:	//U
				this.pressKey(1, 10); break;
			case 106-32:	//J
				this.pressKey(1, 11); break;
		}
	}
	onClickKey(event) {
		// the stupidity of HTML.. event-handler is specifically bound to the SVG but you 
		// get the click coords of the container

		var octWidth= 208;
		var blackKeysHeight= 117;
		var octave= Math.floor(event.clientX / octWidth); // event.clientX relative to enclosing DIV!
		
		var x= event.clientX - octave*octWidth;
		
		// incorrect (due to borders) calculation but good enough for now
		var keyIdIdx;
		if (event.clientY>blackKeysHeight) {
			// only white keys
			var w= octWidth/7;
			keyIdIdx= this.notes2Idx[Math.floor(x/w)];							
		} else {
			// including black keys (group of 5 then group of 7)
			var w= octWidth/(5+7);
			
			if (event.clientY<= w*5) {
				keyIdIdx= Math.floor(x/w);
			} else {
				keyIdIdx= Math.floor((x-5*w)/w) +5;				
			}
		}
		this.pressKey(octave, keyIdIdx);		
	}
	onReleaseKey() {
		this.pressKey(0, null);
	}
}

