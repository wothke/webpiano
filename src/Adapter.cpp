/*
* Piano synthesis based on "Physics-Based Sound Synthesis of the Piano" (Balázs Bank)
* see http://home.mit.bme.hu/~bank/thesis/pianomod.pdf
*
* Emscripten based interface to the JavaScript world. (Quick'n'dirty interface
* to support my 3-voice Pachelbel.)
*
* (c) 2020 Jürgen Wothke
*
* Terms of Use: This software is licensed under a CC BY-NC-SA
* (http://creativecommons.org/licenses/by-nc-sa/4.0/).
*/
#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#include <emscripten.h>

#include "BanksFilters.h"

#include "PianoVoice.h"


#define MAX_SCOPE_BUFFERS 1

namespace piano {

	class Adapter {
	public:
		Adapter() : _numberSamplesRendered(0), _sampleRate(0), _bufSize(0), _scopeBuffer(NULL)
		{
		}

		int loadFile(char *filename, void* inBuffer, uint32_t inBufSize, uint32_t sampleRate, uint32_t audioBufSize, uint32_t scopesEnabled)
		{
			_sampleRate = sampleRate;

			// ignore audioBufSize: always use buffer size corresponding to 50Hz window so that the computeAudioSamples() calls
			// can be used for 50Hz timing (see piano_adapter.js)
			allocBuffers(sampleRate / 50);

			_scopeBuffers[0] = _scopeBuffer;

			if (_pianoVoices[0] == 0)
			{
				_pianoVoices[0] = new PianoVoice( (float)_sampleRate);
				_pianoVoices[1] = new PianoVoice( (float)_sampleRate);
				_pianoVoices[2] = new PianoVoice( (float)_sampleRate);
			}
			return 0;
		}

		void teardown()
		{
			if (_pianoVoices[0] == 0)
			{
				delete _pianoVoices[0]; _pianoVoices[0] = NULL;
				delete _pianoVoices[1]; _pianoVoices[1] = NULL;
				delete _pianoVoices[2]; _pianoVoices[2] = NULL;
			}
		}

		uint32_t getSampleRate()
		{
			return _sampleRate;
		}

		int32_t genSamples()
		{
			if (_pianoVoices[0] == 0)
			{
				_numberSamplesRendered = 0;
			}
			else
			{
				_numberSamplesRendered = _pianoVoices[0]->renderOutput(_soundBufferV1, _bufSize);
				_numberSamplesRendered = _pianoVoices[1]->renderOutput(_soundBufferV2, _bufSize);
				_numberSamplesRendered = _pianoVoices[2]->renderOutput(_soundBufferV3, _bufSize);
			}
			for (int i= 0; i < _numberSamplesRendered; i++) {
				_soundBuffer[i] = _soundBufferV1[i] + _soundBufferV2[i] + _soundBufferV1[i];

				_scopeBuffer[i] = (int16_t)(_soundBuffer[i] * 0x7fff);	// just in case there will be more..
			}

			return _numberSamplesRendered <= 0 ? 1 : 0; // >0 means "end song"
		}

		char* getAudioBuffer()
		{
			return (char*) _soundBuffer;
		}

		uint32_t getAudioBufferLength()
		{
			return _numberSamplesRendered;
		}

		int getNumberTraceStreams()
		{
			return 1;	// just copy regular output
		}

		const char** getTraceStreams()
		{
			return (const char**)_scopeBuffers;
		}


		void strikeNote(int voice, int note, float v0)
		{
			if (_pianoVoices[0] != 0)
			{
				_pianoVoices[voice]->strikeNote(note, v0);
			}
		}

		void setBridgeTweaks(float volume, float g, float decay, float freq, float q, float bridgeZ)
		{
			if (_pianoVoices[0] != NULL)
			{
				_pianoVoices[0]->setBridgeTweaks(volume, g, decay, freq,  q, bridgeZ);
				_pianoVoices[1]->setBridgeTweaks(volume, g, decay, freq,  q, bridgeZ);
				_pianoVoices[2]->setBridgeTweaks(volume, g, decay, freq,  q, bridgeZ);
			}
		}

		void setHammerTweaks(float zh, float vh)
		{
			if (_pianoVoices[0] != NULL) {
				_pianoVoices[0]->setHammerTweaks( zh, vh);
				_pianoVoices[1]->setHammerTweaks( zh, vh);
				_pianoVoices[2]->setHammerTweaks( zh, vh);
			}
		}

		void setStringTweaks(float len, float thickness, float b, float sumZ, float detune, int propagate)
		{
			if (_pianoVoices[0] != NULL)
			{
				bool p = propagate != 0;
				_pianoVoices[0]->setStringTweaks(len, thickness, b, sumZ, detune, p);
				_pianoVoices[1]->setStringTweaks(len, thickness, b, sumZ, detune, p);
				_pianoVoices[2]->setStringTweaks(len, thickness, b, sumZ, detune, p);
			}
		}
	private:
		void allocBuffers(int size)
		{
			if (size > _bufSize)
			{
				if(_scopeBuffer)
				{
					free(_scopeBuffer);
					free(_soundBuffer);
					free(_soundBufferV1);
					free(_soundBufferV2);
					free(_soundBufferV3);
				}
				_scopeBuffer = (int16_t*)malloc(sizeof(int16_t) * size);
				_soundBuffer = (float*)malloc(sizeof(float) * size);
				_soundBufferV1 = (float*)malloc(sizeof(float) * size);
				_soundBufferV2 = (float*)malloc(sizeof(float) * size);
				_soundBufferV3 = (float*)malloc(sizeof(float) * size);

				_bufSize = size;
			}
		}
	private:
		PianoVoice* _pianoVoices[3];	// hardcoded 3 "voices"

		uint32_t _numberSamplesRendered;

		// output "scope" streams corresponding to final audio buffer
		int16_t* _scopeBuffers[MAX_SCOPE_BUFFERS];

		uint32_t _bufSize;
		uint32_t _sampleRate;

		int16_t *_scopeBuffer;

		float *_soundBuffer;

		float *_soundBufferV1;
		float *_soundBufferV2;
		float *_soundBufferV3;
	};
};


static piano::Adapter _adapter;

// old style EMSCRIPTEN C function export to JavaScript.
// todo: code might be cleaned up using EMSCRIPTEN's "new" Embind feature:
// https://emscripten.org/docs/porting/connecting_cpp_and_javascript/embind.html
#define EMBIND(retType, func)  \
	extern "C" retType func __attribute__((noinline)); \
	extern "C" retType EMSCRIPTEN_KEEPALIVE func


	// --- standard APIs (i.e. used by players base impl)
EMBIND(int, emu_load_file(char *filename, void* inBuffer, uint32_t inBufSize, uint32_t sampleRate, uint32_t audioBufSize, uint32_t scopesEnabled))
														{ return _adapter.loadFile(filename, inBuffer, inBufSize, sampleRate, audioBufSize, scopesEnabled); }
EMBIND(void, emu_teardown())							{ _adapter.teardown(); }
EMBIND(int, emu_compute_audio_samples())				{ return _adapter.genSamples(); }
EMBIND(int, emu_number_trace_streams())					{ return _adapter.getNumberTraceStreams(); }
EMBIND(const char**, emu_get_trace_streams())			{ return _adapter.getTraceStreams(); }
EMBIND(char*, emu_get_audio_buffer())					{ return _adapter.getAudioBuffer(); }
EMBIND(int32_t, emu_get_audio_buffer_length())			{ return _adapter.getAudioBufferLength(); }
EMBIND(int, emu_get_sample_rate())						{ return _adapter.getSampleRate(); }

   // --- piano specific add-ons
EMBIND(void, emu_strike_note(int voice, int note, float v0))													{ _adapter.strikeNote(voice, note, v0); }
EMBIND(void, emu_set_bridge_tweaks(float volume, float g, float decay, float freq, float q, float bridgeZ))		{ _adapter.setBridgeTweaks(volume, g, decay, freq, q, bridgeZ); }
EMBIND(void, emu_set_hammer_tweaks(float zh, float vh))															{ _adapter.setHammerTweaks(zh, vh); }
EMBIND(void, emu_set_string_tweaks(float len, float thickness, float b, float sumZ, float detune, int propagate))	{ _adapter.setStringTweaks(len, thickness, b, sumZ, detune, propagate); }

