# webPiano

(c) 2020 Jürgen Wothke


This toy project is the result of my first exploration into the realm of synthesized
piano sounds. My goal was to get some hands-on experience with a respective "digital waveguide"
based design and to learn a thing or two in the process.

The implementation is based on Balázs Bank's "Physics-Based Sound Synthesis of the Piano"
(see http://home.mit.bme.hu/~bank/thesis/pianomod.pdf) and the various papers that are cross
referenced in that thesis (see respective references in the code comments). 


A live demo can be found here: https://www.wothke.ch/webPiano



# License

Terms of Use: This software is licensed under a CC BY-NC-SA 
(http://creativecommons.org/licenses/by-nc-sa/4.0/). 