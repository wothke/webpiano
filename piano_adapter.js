/*
 piano_adapter.js: experimenting with "waveguide" based piano synth.

 version 0.02

 	Copyright (C) 2020-2023 Juergen Wothke

 LICENSE

 This software is licensed under a CC BY-NC-SA
 (http://creativecommons.org/licenses/by-nc-sa/4.0/).
*/
class PianoBackendAdapter extends EmsHEAPF32BackendAdapter {
	constructor(nextFrameCB)
	{
		super(backend_PIANO.Module, 1, undefined, new HEAP16ScopeProvider(backend_PIANO.Module, 0x8000));	// mono

		this._nextFrameCB= (typeof nextFrameCB == 'undefined') ? function() {} : nextFrameCB;

		this.ensureReadyNotification();
	}

	// disable default impls
	skipFileLoad() 						{ return 1; }	// disable file retrieval
	evalTrackOptions(options)			{ return 0; }

	loadMusicData(sampleRate, path, filename, data, options)
	{
		// setup the piano emulation
		let ret = this.Module.ccall('emu_load_file', 'number',
							['string', 'number', 'number', 'number', 'number', 'number'],
							[ 0, 0, 0, ScriptNodePlayer.getWebAudioSampleRate(), -999, true]);

		if (ret == 0)
		{
			let inputSampleRate = this.Module.ccall('emu_get_sample_rate', 'number');
			this.resetSampleRate(sampleRate, inputSampleRate);
		}
		return 0;
	}

	computeAudioSamples()
	{
		// hack: called with 50Hz freq - see buffer size used in Adapter.cpp
		this._nextFrameCB(this);				// used for "interactive mode"
		return super.computeAudioSamples();
	}


// --- APIs to interact with the piano emulation

	strikeNote(voice, note, v)
	{
		this.Module.ccall('emu_strike_note', 'number', ['number','number','number'], [voice, note, v]);
	}

	setHammerTweaks(zh, vh)
	{
		this.Module.ccall('emu_set_hammer_tweaks', 'number', ['number','number'], [zh, vh]);
	}

	setBridgeTweaks(volume, g, decay, freq, q, bridgeZ)
	{
		this.Module.ccall('emu_set_bridge_tweaks', 'number', ['number','number','number','number','number','number'], [volume,g, decay, freq, q, bridgeZ]);
	}

	setStringTweaks(len, thickness, b, sumZ, detune, propagate)
	{
		this.Module.ccall('emu_set_string_tweaks', 'number', ['number','number','number','number','number','number'], [len, thickness, b, sumZ, detune, propagate]);
	}
	
	// disable unsupported default impls
	
	getMaxPlaybackPosition()
	{
		return -1;
	}
	getPlaybackPosition()
	{
		return -1;
	}
	seekPlaybackPosition(ms)
	{
	}
};

